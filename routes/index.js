var express = require('express');
var jwt = require('jsonwebtoken')
var router = express.Router();

router.get('/', (req, res) => {
  res.json({message: 'This is API'})
})

router.post('/posts', vaerifyToken,{expiresIn: '1d'}, (req, res) => {

  //authentication
  jwt.verify(req.token, 'akusayangkamu', (err, authData) => {
    if(err) {
      res.sendStatus(403)
    } else {
      res.json({
        message: 'Posts Created',
        authData
      })
    }
  })

})

router.post('/login', vaerifyToken, (req, res) => {
  //Mock user
  const user = {
    id : 1,
    username: 'rahadian',
    email: 'mrahadian.permana@gmail.com'
  }

  jwt.sign({user}, 'akusayangkamu', (err, token) => {
    res.json({
      token
    })  
  })
})

//middleware
function vaerifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ')
    //get Token
    const bearerToken = bearer[1]
    //Set Token
    req.token = bearerToken
    //next middleware
    next()
    
  } else {
    res.sendStatus(403)
  }
}

module.exports = router;
